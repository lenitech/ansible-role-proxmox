auto_auth {
  method "approle" {
    config = {
      role_id_file_path = "/etc/vault.d/role-id"
      secret_id_file_path = "/etc/vault.d/secret-id"
      remove_secret_id_file_after_reading = false
    }
  }
}

template_config {
  exit_on_retry_failure = true
  static_secret_render_interval = "5m"
}

template {
  source      = "/etc/vault.d/cert.ctmpl"
  destination = "/etc/ssl/certs/proxmox.pem"
  perms       = "0644"
  error_on_missing_key = true
}

template {
  source      = "/etc/vault.d/domains.cfg.ctmpl"
  destination = "/etc/pve/domains.cfg"
  perms       = "0640"
  user        = "root"
  group       = "www-data"
  error_on_missing_key = true
}
